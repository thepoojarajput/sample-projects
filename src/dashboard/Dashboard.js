
import React, { useState, useContext, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {UserContext} from '../App';
import {auth} from '../firebase/firebase';
import { useFirestoreConnect, isLoaded } from 'react-redux-firebase';
import {useSelector} from 'react-redux';
import MUIDataTable from 'mui-datatables';

export default function Dashboard(){

    const context = useContext(UserContext);
    console.log(useContext(UserContext))
    const [user, setUser] = useState(context.user);
    const [list, setList] = useState([])
    const history = useHistory();

    // useFirestoreConnect([
    //     {collection: 'users'}
    // ])

    // const users = useSelector(state => state.firestore.data.users);

    const handleSignOut = () => {
        auth.signOut().then(function(){
            context.dispatch({type: 'LOG_OUT'})
        }).catch()
    }

    useEffect(() => {
        auth.onAuthStateChanged(function(currentUser){
            if (currentUser){
                setUser({ name: currentUser.displayName, email: currentUser.email})
            }else{
                history.push("/")
            }
        })
        // if (isLoaded(users)){
        //     if(users){
        //         let data = []
        //         const list = Object.keys(users).map(i => 
        //             data[i] = {
        //                 ...users[i]
        //             }
        //         );
        //         setList(list)
        //     }
        // }
    }, [])

    const columns = [
        {
            name: 'name',
            label: 'Name'
        },
        {
            name: 'email',
            label: 'Email'
        }
    ]



    return(
        <div className="container">
            <div align="right">
                <nav>
                    <Link to="/" onClick={handleSignOut} >Sign Out</Link>
                </nav>
            </div>
            <center>
                <h1> Hello! {user.email} </h1>
            </center>
            {/* <MUIDataTable 
                columns = {columns}
                // options = {options}
                data = {list} /> */}
        </div>
    )
}