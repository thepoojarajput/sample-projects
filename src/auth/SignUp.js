import React, {useState, useEffect, useContext} from 'react';
import {Link, useHistory} from "react-router-dom";
import {auth, provider, database} from '../firebase/firebase';
import {UserContext} from '../App'

export default function SignUp(props){

    const [user, setUser] = useState({});
    const [error, setError] = useState('');
    const history = useHistory();
    const context = useContext(UserContext);

    const handleChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }
    const handleSubmit = async () => {
        const {name, email, password} = user;
        if(name && email && password){
            await auth.createUserWithEmailAndPassword(email, password).catch(err => {
                let errorCode = err.code;
                let errorMessage = err.message;
                console.log(errorCode, errorMessage);
                switch(errorCode){
                    case 'auth/network-request-failed': return setError('Please check your Internet Connection!');
                    case 'auth/email-already-in-use': return setError('This email is already registered. Please use a different email-id');
                    case 'auth/weak-password': return setError(errorMessage);
                    default: return null;
                }
            });
            const currentUser = auth.currentUser;
            if(currentUser){
                setError('')
                auth.currentUser.updateProfile(
                    {
                        displayName: name
                    }
                )
                database
                    .collection("users")
                    .add(user)
                    .then(history.push("/"))
            }
        }
    }
    useEffect(() => {
        auth.onAuthStateChanged(function (currentUser) {
            if (currentUser) {
                setUser({ name: currentUser.displayName, email: currentUser.email })
                context.dispatch({ name: currentUser.displayName, email: currentUser.email })
                history.push('/dashboard')
            }
        })
    }, [])

    return(
        <div>
            <center>
                <span style={{color: 'red'}}>{error}</span>
                <h1> Sign Up </h1> 
            </center>
                <div>
                    <div className="row">
                        <label> Name <span style={{ color: 'red' }}> * </span> </label>
                        <input type="text" name="name" value={user.name} 
                            onChange={e => handleChange(e)} />
                    </div>
                    <div className="row">
                        <label> Email <span style={{color: 'red' }}> * </span> </label>
                        <input type="text" name="email" value={user.email}
                            onChange={e => handleChange(e)} />
                    </div>
                    <div className="row">
                        <label> Password <span style={{ color: 'red' }}> * </span> </label>
                        <input type="text" name="password" value={user.password}
                            onChange = {e => handleChange(e)} />
                    </div>
                    <div className="row">
                        <Link to='./'>Already Have An Account?</Link>
                    </div>
                    <div className="row">
                        <span style={{ color: 'red' }}>{error}</span>
                        <button onClick = {handleSubmit}>Sign Up</button>
                    </div>
                    <div className="row">
                        <button
                            onClick={() => { props.handleGoogleLogin(context); history.push('/dashboard') }}>
                                Sign In With Google
                        </button>
                    </div>
                </div>
        </div>
    )
}