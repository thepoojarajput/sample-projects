import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {auth} from '../firebase/firebase';

export default function ForgotPassword(){

    const [email, setEmail] = useState('');

    const history = useHistory();

    const handleChange = (e) => {
        setEmail(e.target.value)
    }

    var actionCodeSettings = {
        // URL you want to redirect back to. The domain (www.example.com) for this
        // URL must be whitelisted in the Firebase Console.
        url: 'http://localhost:3000/dashboard',
        // This must be true.
        handleCodeInApp: true,
        
        dynamicLinkDomain: 'example.page.link'
    };

    const handleSubmit = () => {
        // auth.sendPasswordResetEmail(email, '', )
        history.pushState('/')
    }

    return(
        <div className = "container">
            <form>
                <div className ="row">
                    <div className = "col md 5">
                        <label>Email <span style ={{ color: "red"}}>* </span></label>
                        <input type="email" autoFocus name="email" value={email}
                            onChange = {handleChange} />
                    </div>
                    <div>
                        <button onClick={handleSubmit}>Send Verification Code</button>
                    </div>
                </div>
            </form>
        </div>
    )
}