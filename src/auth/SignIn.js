import React, { useState, useEffect, useContext } from 'react';
import {Link } from "react-router-dom";
import {database, auth, provider} from "../firebase/firebase";
import { useHistory } from "react-router-dom";
import {UserContext} from '../App';


export default function SignIn(props) {

    const [user, setUser] = useState({});
    const [error, setError] = useState('');
    const history= useHistory();

    const context = useContext(UserContext);

    const handleChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }
    const handleSubmit = async () => {
        const {email, password} = user
        if(email && password){
            await auth.signInWithEmailAndPassword(email, password).catch(err => {
                let errorCode = err.code;
                let errorMessage = err.message;
                console.log(errorCode, errorMessage);
                switch (errorCode) {
                    case 'auth/network-request-failed': return setError('Please check your Internet Connection!');
                    case 'auth/user-not-found': return setError('This email is not registered!');
                    case 'auth/wrong-password': return setError('Please check your password.');
                    default: return null;
                }
            })
            const currentUser = auth.currentUser;
            if(currentUser){ 
                setUser({
                    ...user,
                    name: currentUser.displayName
                })
                context.dispatch({type: 'AUTH_SUCCESS', payload: user})
                console.log(user)
                history.push('/dashboard') 
            }

        }
    }

    useEffect (() => {
        auth.onAuthStateChanged(function (currentUser) {
            if (currentUser) {
                setUser({ name: currentUser.displayName, email: currentUser.email })
                context.dispatch({ name: currentUser.displayName, email: currentUser.email })
                history.push('/dashboard')
            }
        })
    }, [])

    return (
            <div>

                {/* <div className="row">
                    <button
                        style = {{ width: 80}}
                        onClick={() => { props.handleGoogleLogin(context); history.push('/dashboard') }}>
                        Click Ad
                    </button>
                </div> */}
                <center>
                    <span style={{ color: 'red' }}>{error}</span>
                    <h1> Sign In </h1>
                </center>
                    <div>
                        <div className="row">
                            <label> Email <span style={{ color: 'red' }}> * </span> </label>
                            <input type="text" name="email" value={user.email} autoFocus
                                onChange={e => handleChange(e)} />
                        </div>
                        <div className="row">
                            <label> Password <span style={{ color: 'red' }}> * </span> </label>
                            <input type="text" name="password" value={user.password}
                                onChange={e => handleChange(e)} />
                        </div>
                        <div className ="row">
                            <Link to='./SignUp'>Create An Account</Link>
                        </div>
                        <div className="row">
                            <Link to='./ForgotPassword'>Forget Password</Link>
                        </div>
                        <div className="row">
                            <span style={{ color: 'red' }}>{error}</span>
                            <button onClick={handleSubmit}>Sign In</button>
                        </div>
                        <div className="row">
                            <button 
                                onClick={() => { props.handleGoogleLogin(context); history.push('/dashboard')}}>
                                    Sign In With Google
                            </button>
                        </div>
                    </div>
            </div>
    )
}