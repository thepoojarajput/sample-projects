import React, {useContext} from 'react';
import { Link, Route, BrowserRouter as Router} from 'react-router-dom';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Dashboard from '../dashboard/Dashboard'; 
import '../App.css';
import {auth, database, provider} from '../firebase/firebase';
// import {UserContext} from '../App';
import ForgotPassword from './ForgotPassword';

export default function Nav(){

    // const context = useContext(UserContext);

    const handleGoogleLogin = (context) => {
        auth.signInWithPopup(provider)
            .then(async (result) => {
                if (result.credential) {
                    const currentUser = result.user;
                    var token = result.credential.accessToken;
                    const newUser = {
                        name: currentUser.displayName,
                        email: currentUser.email
                    }
                    await database
                        .collection("users")
                        .add(newUser)
                        .then()
                    context.dispatch({ type: 'AUTH_SUCCESS', payload: newUser })
                    
                }

            });
    }

    return(
        <div className ="container">
            <Router>
            {/* <nav>
                <Link to="/">SignIn</Link>
                <Link to="/SignUp">Sign Up</Link>
                <Link to="/dashboard">Dashboard</Link>
            </nav> */}

            <Route path="/" component = { () => <SignIn handleGoogleLogin = {handleGoogleLogin}/>} exact />
            <Route path ="/SignUp" component = {() => <SignUp handleGoogleLogin = {handleGoogleLogin} />} exact />
            <Route path= "/dashboard" component = {Dashboard} exact />
            <Route path="/ForgotPassword" component={ForgotPassword} exact />
            </Router>
        </div>
    )
}