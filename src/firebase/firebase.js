import firebase from 'firebase';


const config = {
    apiKey: "AIzaSyBpQnxQ6b79AXt-8MZCi4F2FzbuoTALSSo",
    authDomain: "samplecg2-0.firebaseapp.com",
    databaseURL: "https://samplecg2-0.firebaseio.com",
    projectId: "samplecg2-0",
    storageBucket: "samplecg2-0.appspot.com",
    messagingSenderId: "82098484639",
    appId: "1:82098484639:web:58380c76d214fd03af79aa",
    measurementId: "G-WDSDTBBBBS"
  };
  firebase.initializeApp(config);

  const database = firebase.firestore();
  const provider = new firebase.auth.GoogleAuthProvider();
  const auth = firebase.auth();
  
  export {database, provider, auth}