import React, {createContext} from 'react';
import Nav from './auth/index';

export const UserContext = createContext()

const initialUser = {
  name: '', email: ''
}
const reducer = (state, action) => {
  switch(action.type){
    case 'AUTH_SUCCESS': return { name: action.payload.name, email: action.payload.email}
    case 'LOG_OUT': return {name: '', email: ''}
    default : return state
  }
}

function App() {
  const [user, dispatch] = React.useReducer(reducer, initialUser);

  return (
    <div className="App">
      <UserContext.Provider value={{user: user, dispatch: dispatch}}>
        <Nav />
      </UserContext.Provider>
    </div>
  );
}

export default App;
